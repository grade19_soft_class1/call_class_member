﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp13
{
    class Program
    {
        static void Main(string[] args)
        {
            Book book = new Book();
            book.Name = "《复活》";
            book.Price = 36;
            book.Number = 183;
            book.BookInfo();
        }
    }
}
