﻿using System;

namespace ConsoleApp6
{
    class Count
    {
        public void Add(int a, int b)
        {
            Console.WriteLine("{0}*{1}={2}", a, b, a * b);
        }
    }
}