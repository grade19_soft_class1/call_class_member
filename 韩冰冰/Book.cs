﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp7
{
    class Book
    {
        public string Name { get; set; }
        public int Price { get; set; }
        public int Number { get; set; }

        public void BookInfo()
        {
            Console.WriteLine("书名"+Name);
            Console.WriteLine("价格"+Price);
            Console.WriteLine("数量"+Number);
        }
    }
}
