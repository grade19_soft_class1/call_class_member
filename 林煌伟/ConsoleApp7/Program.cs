﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp7
{
    class Program
    {
        static void Main(string[] args)
        {
            //实例化
            Book book = new Book();

            //赋值
            book.name = "诗经";

            Console.WriteLine("请输入书的价格");
            //输入并赋值
            book.price = int.Parse(Console.ReadLine());

            Console.WriteLine("请输入数的剩余数量");
            //输入并赋值
            book.number= int.Parse(Console.ReadLine());

            //调用方法一
            book.BookInfo();
           
        }
    }
}
