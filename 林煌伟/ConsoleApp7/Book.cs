﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp7
{
     public class Book
    {
        //定义变量
        public string name { get; set; }
        public int price { get; set;  }
        public int number { get; set;  }

        //方法一（输出）
        public void BookInfo()
        {
            Console.WriteLine("书名是《{0}》，它的价格为：{1}，它剩余的数量为：{2}",name,price,number);   
        }

    }
}
