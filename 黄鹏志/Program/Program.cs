﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            // 创建一个新的 Student 对象
            Student s = new Student();

            // 设置 student 的 code、name 和 age
            s.Code = "1314520xb";
            s.Name = "小志";
            s.Age = 19;
            Console.WriteLine("学生: {0}", s);
            Console.ReadKey();
        }
    }
}
