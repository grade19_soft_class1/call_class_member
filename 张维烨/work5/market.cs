﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace work5
{
    class Market
    {
        private string name;
        private int price;

        public string Name { get { return name; } set { name = value; } }
        public int Price { get { return price; } set { price = value; } }

        public void PrintMesg(string name, int price) {
            this.Name = name;
            this.Price = price;
            Console.WriteLine("{0},{1}元/斤",this.Name,this.Price);
        }
    }
}
