﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Girl
    {
    
        private string name;
        private int age;
        private int height;
        private int weight;
        //名字
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }
        //年龄
        public int Age
        {
            get
            {
                return age;
            }
            set
            {
                if(value>=0)
                {
                    age = value;
                }
                else
                {
                    age = 0;
                }
            }
        }
        //身高
        public int Height
        {
            get
            {
                return height;
            }
            set
            {
                height = value;
            }
        }
        //体重
        public int Weight
        {
            get
            {
                return weight;
            }
            set
            {
                weight = value;
            }
        }
        public void GirlInfo()
        {
            Console.WriteLine("有一个姑娘她叫{0}，她今年{1}岁了，她有{2}高，重达{3}",name,age,height,weight);
        }


    }
}
