﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp2
{
    public class Book
    {
        public int id;//设置编号
        public string name;//设置名称
        public double price;//设置价格
        //编号属性设置
        public int Id
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
            }
        }
        //名称属性设置
        public string Name
        {
            get
            {
                return name;
            }
            
        }
        //价格属性设置
        public double Price
        {
            get
            {
                return price;
            }
            set
            {
                if(value<=0)
                {
                    price = 0.00;
                }
                else
                {
                    price = value;
                }
            }
        }
        public void printbook()
        {
            Console.WriteLine($"书本编号:{Id}    书本名称：{Name}    书本价格：{Price}元");
        }

    }
}
