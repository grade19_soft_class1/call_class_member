﻿using System;

namespace Studen2
{
    class Program
    {
        static void Main(string[] args)
        {
            Studen2 zhangsan = new Studen2("张三",18,"男",116,55,27);
            zhangsan.Name = "张三";
            zhangsan.Age = 18;
            zhangsan.Sex = "男";
            zhangsan.Chinese = 116;
            zhangsan.English = 55;
            zhangsan.Math = 27;
            zhangsan.SayHello();
            zhangsan.score();

            Studen2 wanggang = new Studen2("王刚",19,"男",126,75,60);
            
            wanggang.SayHello();
            wanggang.score();
            Console.ReadKey();    //代码冗余  解决方法：构造函数 构造函数：构造函数没有返回值，连void也不能写。构造函数的名称必须和类名一样。构造函数访问修饰符必须是public

            //new 关键字 的作用
            //1.在内存中开辟空间
            //2.在开辟的空间中创建对象
            //3.调用对象的构造函数进行初始化对象


            //用this关键字 用构造函数调用构造函数
            Studen2 laoliu = new Studen2("老刘",123,251,66);
            Console.ReadKey();
        }
    }
}
