﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace Studen2
{
    class Studen2
    {
        //析构函数  析构函数当程序结束后才执行，析构函数帮助我们释放资源  如果希望马上释放资源就使用析构函数
        ~Studen2()
        {
            Console.WriteLine("我是析构函数");
        }




        //构造函数          创建对象的时候会执行构造函数
        public Studen2(string name,int age,string sex,int chinese,int english , int math){
            this.Name = name;
            this.Age = age;
            this.Sex = sex;
            this.Chinese = chinese;
            this.English = english;
            this.Math = math;
            }

        //this关键字   构造函数调用另一个构造函数
        //this有两个作用：1.代表当前类的对象   2.在类中现实的调用本类的构造函数 语法 :this
        public Studen2(string name, int english, int chinese, int math ):this(name,0,"c",chinese,english,math)
        {
            //this.Name = name;
            //this.Age = age;
            //this.Sex = sex;
            //this.Chinese = chinese; 
            //this.English = english;
            
        }





        public Studen2 (string name,int age,string sex)
        {
            //构造函数是可以有重载的
            this.Name = name;
            this.Age = age;
            this.Sex = sex;
        }


        private string _name;
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private int _age;
        public int Age
        {
            get { return _age; }
            set { 
                if(value < 0 || value > 100)
                {
                    value = 0;
                }
                _age = value; }
        }

        private string _sex;
        public string Sex
        {
            get { 
                if(_sex != "男" &&  _sex != "女")
                {
                    return _sex = "男";
                }
                return _sex; }
            set { _sex = value; }
        }

        private  int  _chinese;
        public int  Chinese
        {
            get { return _chinese; }
            set { _chinese = value; }
        }

        private int  _english;
        public int  English
        {
            get { return _english; }
            set { _english = value; }
        }

        private int  _math;
        public int Math
        {
            get { return _math; }
            set { _math = value; }
        }

        public void SayHello()
        {
            Console.WriteLine("我叫{0},我{1}岁,我是{2}生",this.Name,this.Age,this.Sex);
        }

        public void score()
        {
            Console.WriteLine("我的名字是{0},我的总成绩是{0},我的平均成绩是{1}",this.Name,this.Chinese + this.English + this.Math,(this.Chinese + this.English + this.Math)/3);
        }

    
        
    }
}
