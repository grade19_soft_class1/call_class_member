﻿using System;

class Person
{
    public string Name;
    public Int32 Age;
    public string Profession;
    public string City;
    public void SayHello()
    {
        Console.WriteLine("Hello! My name is " + Name);
    }
    public void Introduce()
    {
        Console.WriteLine("Hello! My age is " + Age);
    }
    public void Interview()
    {
        Console.WriteLine("Hello! My profession is " + Profession);
    }
    public void LiveCity()
    {
        Console.WriteLine("Hello! I live in " + City);
    }
    public static void Main(string[] args)
    {
        Person p = new Person();
        p.City = "FuJian Province";
        p.Profession = "Compute";
        Console.WriteLine(p.City);
        Console.WriteLine(p.Profession);

        p.Age = 25;
        p.Name = "Joker";
        Console.WriteLine(p.Age);
        Console.WriteLine(p.Name);

        p.Introduce();
        p.SayHello();
        p.Interview();
        p.LiveCity();
    }
}

