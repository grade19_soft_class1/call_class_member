﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework
{
    class Program
    {
        static void Main(string[] args)
        {
            Student mike = new Student();
            mike.Name = "迈克";
            mike.Age = 7;
            mike.Gender = '男';
            mike.Chinese = 98;
            mike.English = 97;
            mike.Describe();
            mike.Score();
            Console.WriteLine();
            Student jane = new Student();
            jane.Name = "简";
            jane.Age = 18;
            jane.Gender = '女';
            jane.Chinese = 98;
            jane.English = 99;
            jane.Describe();
            jane.Score();
        }
    }
}
