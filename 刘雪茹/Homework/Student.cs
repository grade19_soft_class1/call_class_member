﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework
{
    class Student
    {
        private string _name;
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private int _age;
        public int Age
        {
            get { return _age; }
            set
            {
                if (value < 0 || value > 130)
                {
                    value = 0;
                }
                _age = value;
            }
        }

        private char _gender;
        public char Gender
        {
            get { return _gender;}
            set { _gender = value;}
        }

        private int _chinese;
        public int Chinese
        {
            get { return _chinese; }
            set { _chinese = value; }
        }

        private int _english;
        public int English
        {
            get { return _english; }
            set { _english = value; }
        }

        public void Describe()
        {
            Console.WriteLine("我叫{0},今年{1}岁了,是{2}生",this.Name,this.Age,this.Gender);
        }

        public void Score()
        {
            Console.WriteLine("我叫{0},总成绩:{1}分,平均成绩:{2}分",this.Name,this.Chinese+this.English,( this.Chinese + this.English)/2);
        }
    }
}
