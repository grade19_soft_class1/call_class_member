﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp5
{
    class Flower
    {
        private string name;
        private double price;
        private int id;
        
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }
        public double Price
        {
            get
            {
                return price;
            }
            set
            {
                if (value >= 0)
                {
                    price = value;
                }
                else
                {
                    price = 0;
                }
            }
        }
        public int Id
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
            }
        }
        public  void FlowerInfo()
        {
            Console.WriteLine("花的名字：{0} 花的价格{1} 花的编号{2}",this.name,this.price,this.id);
        }
        public void InputFlowerInfo(string name,double price,int id)
        {
            Name = name;
            Price = price;
            Id = id;
        }
    }
}
