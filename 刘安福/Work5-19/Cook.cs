﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Work5_19
{
    class Cook
    {
        public static int CookId { get; set; }

        public static string CookName { get; set; }

        public static int CookPrice { get; set; }


        public static void PrintCook()
        {
            Console.WriteLine("食物编号：{0}，食物名字：{1}，食物价格：{2}",CookId,CookName,CookPrice);
        }

        public static void SetCook(int cookId, String cookName, int cookPrice)
        {
            CookId =cookId;//命名规则
            CookName = cookName;
            CookPrice = cookPrice;
        }

    }
}
