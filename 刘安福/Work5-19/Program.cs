﻿using System;

namespace Work5_19
{
    class Program
    {
        static void Main(string[] args)
        {
            Book book = new Book();//new一个


            book.Setup(111111, "钢铁侠是怎样养成的", 99);

            //快捷注释 ctrl+k...+ c  取消 k+u  

            //book.Id = 11111;

            //book.Title = "钢铁侠是怎样练成的";

            //book.Price = 3;

            //book.Price = 2;//单独调价格

            book.Print();
            
            //食物

            Console.WriteLine();

            Cook cook = new Cook();

            Cook.SetCook(1111, "葱油大饼", 10);

            //Cook.CookId = 11;
            //Cook.CookName = "葱花大饼";
            //Cook.CookPrice = 10;


            Cook.PrintCook();
        }
    }
}
