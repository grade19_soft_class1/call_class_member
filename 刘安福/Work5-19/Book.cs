﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Work5_19
{
    public class Book  //都可用
    {
        private string title;

        public int _id;

        private double price;//图书价格

        //图书标题
        public string Title
        {
            get
            {
                return title;
            }
            set
            {
                title = value;
            }
        }

        //图书编号

        public int Id
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
            }
        }

       //图书价格
        public double Price
        {
            get
            {
                return price;
            }
            set
            {
                //对图书价格作出要求
                if (value >= 0)
                {
                    price = value;
                }
                else
                {
                    price = 0;
                }
            }

        }

        public void Print()
        {
            Console.WriteLine("书的名字:《{0}》 ~光棍指数：{1} ~ 书的价格：{2}", this.title, _id, this.price);//直接使用字段
        }
        //this  当前对象
        public void Setup(int id, string title, int price)
        {
            _id = id;
            this.title = title; 
            this.price = price;
        }

        public string NickName { get; set; }  //prop 自动属性
    }
}
