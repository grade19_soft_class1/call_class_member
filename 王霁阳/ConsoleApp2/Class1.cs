﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    public class Book
    {
        private int id;

        private string name;
        private double _age;

        public int Id 
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
            }
        }
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }
        public double  Age
        {
            
            get
            {
                return _age;
            }
            set
            {
                if (value == 0)
                {
                    Console.WriteLine("你的书不要钱吗");
                    _age = value;
                }else if (value < 0)
                {
                    Console.WriteLine("倒贴？？那是真的牛皮");
                    _age = value;
                }
                else
                {
                    _age = value;
                }
            }
        }
        public string NickName { get; set; }
        public void PrintBookOf()
        {
            Console.WriteLine("书的Id:{0} 书的名字:{1} 书的价格:{2}",this.id,this.name,this._age);
        }
        public void printbook(int Id, string Name, double price)
        {
            Console.WriteLine($"书籍编号{Id}  书籍名字{Name}  书籍价格{price}");
        }

    }
}
