﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 练习_调用类成员
{
   public static class Student
    {
        private static int _id;

        public static int ID
        {
            get
            {
                return _id;

            }
            set
            {
                _id = value;

            }
        }

        private static string _name;

        public static string Name
        {
            get
            {
                return _name;

            }
            set
            {
                _name = value;

            }
        }

        private static int _age;

        public static int Age
        {
            get
            {
                return _age;

            }
            set
            {
                _age = value;

            }
        }
        public static void PrintStudentInfo()
        {
            Console.WriteLine("学号：{0}，姓名：{1}，年龄：{2}",ID,Name,Age);
        }
        
    }
}
