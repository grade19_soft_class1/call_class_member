﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApplication4
{
    class Book
    {
        public string Title { get;set; }

        public int BookId {get ;set;}

        public int Price { get; set; }

        public  void PrintBook()
        {
            Console.WriteLine("书名：{0},书本编号：{1}，书价：{2}",Title,BookId,Price);
        }

        public void SetBook(string title, int bookid, int price)
        {
            Title = title;
            BookId = bookid;
            Price = price;
        }
    }
}
