﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo06
{
    public class Ticket
    {
        private double distance;
        public double Distance
        {
            get { return distance; }
        }
        public Ticket(double distance)
        {
            if (distance < 0)
            {
                distance = 0;
            }
            this.distance = distance;
        }
        private double price;
        public double Price
        {
            get
            {
                if (distance > 0 && distance < 100)
                {
                    return distance * 1.0;
                }
                else if (distance > 101 && distance < 200)
                {
                    return distance * 0.95;
                }
                else if (distance > 201 && distance < 399)
                {
                    return distance * 0.9;
                }
                else
                {
                    return distance * 0.8;
                }
              
            }
           
        }
        public void Showinfo()
        {
            Console.WriteLine("欢迎来到根据距离计算价格系统 \n您本次行驶了{0}km 花费了{1}元",this.Distance ,this .Price  );
        }



    }
}
