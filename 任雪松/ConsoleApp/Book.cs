﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Book
    {
        public int Price { get; set; }
        public string Name { get; set; }

        public void Take(int price, string name)
        {
            this.Price = price;
            this.Name = name;
        }
        public void Aircraft()
        {
            Console.WriteLine("书名为：《{0}》,书价：{1}", Name, Price);
        }
    }
}
