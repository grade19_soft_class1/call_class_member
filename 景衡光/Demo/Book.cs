﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Demo
{
    class Book
    {
        public int  Price { get; set; }
        public string Name { get; set; }

        public void Enter(int price ,string name)
        {
            this.Price = price;
            this.Name = name;
        }
        public void Getup()
        {
            Console.WriteLine("书名：《{0}》,书的价格：{1}美元", Name,Price);
            Console.WriteLine("你值得拥有");
        }
    }
}
