﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp7
{
    class Book
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public void SetBook(int id, string name)
        {
            Id = id;
            Name = name;
        }
        public void PrintMsg()
        {
            Console.WriteLine("图书编号："+Id);
            Console.WriteLine("图书名称："+Name);
        }

    }
}
