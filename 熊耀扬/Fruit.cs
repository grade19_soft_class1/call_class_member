﻿using System;
using System.Collections.Generic;
using System.Reflection.Metadata.Ecma335;
using System.Text;

namespace ConsoleApp3
{
    class Fruit
    {
        private string name;
        private int price;
        private int weight;
        public string Name
        {
            get
            {
                return name;
            }
            set 
            {
                name = value;
            }
        }
        public int Price
        {
            get
            {
                return price;
            }
            set
            {
                if (value >= 0)
                {
                    price = value;
                }
                else
                {
                    price = 0;
                }
            }
        }
        public int Weight
        {
            get
            {
                return weight;
            }
            set
            {

                if (value >= 0)
                {
                    weight = value;
                }
                else
                {
                    weight = 0;
                }
            }
        }
        public void FruitInfo()
        { 
        Console.WriteLine("水果名字: {0} 水果价格: {1}元/斤 水果重量: {2}克",this.name,this.price,this.weight);
        }
        public void InputtingFruitInfo(string name,int price,int weight)
        {
            Name = name;
            Price = price;
            Weight = weight;
        }
    }
}
